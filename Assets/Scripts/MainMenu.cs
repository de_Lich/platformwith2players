using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject optionsMenu;
    public GameObject mainMenu;

    Event keyEvent;
    Text buttonText;
    KeyCode newKey;

    bool waitingForKey;

    private void Start()
    {
        Time.timeScale = 1;
        optionsMenu.SetActive(false);
        mainMenu.SetActive(true);
        waitingForKey = false;

        for (int i = 0; i <= 7; i++)
        {
            switch (optionsMenu.transform.GetChild(i).name)
            {
                case "ShootermoveRightKey":
                    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.ShooterRight.ToString();
                    break;
                case "ShootermoveLeftKey":
                    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.ShooterLeft.ToString();
                    break;
                case "ShooterJumpKey":
                    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.ShooterJump.ToString();
                    break;
                case "ShootKey":
                    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.Shoot.ToString();
                    break;
                case "TankRightKey":
                    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.TankRight.ToString();
                    break;
                case "TankLeftKey":
                    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.TankLeft.ToString();
                    break;
                case "TankJumpKey":
                    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.TankJump.ToString();
                    break;
            }
            //if (optionsMenu.transform.GetChild(i).name == "ShootermoveRightKey")
            //    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.ShooterRight.ToString();
            //else if (optionsMenu.transform.GetChild(i).name == "ShootermoveLeftKey")
            //    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.ShooterLeft.ToString();
            //else if (optionsMenu.transform.GetChild(i).name == "ShooterJumpKey")
            //    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.ShooterJump.ToString();
            //else if (optionsMenu.transform.GetChild(i).name == "ShootKey")
            //    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.Shoot.ToString();
            //else if (optionsMenu.transform.GetChild(i).name == "TankRightKey")
            //    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.TankRight.ToString();
            //else if (optionsMenu.transform.GetChild(i).name == "TankLeftKey")
            //    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.TankLeft.ToString();
            //else if (optionsMenu.transform.GetChild(i).name == "TankJumpKey")
            //    optionsMenu.transform.transform.GetChild(i).GetComponentInChildren<Text>().text = GameManager.GM.TankJump.ToString();
        }
    }
    private void OnGUI()
    {
        keyEvent = Event.current;

        if (keyEvent.isKey && waitingForKey)
        {
            newKey = keyEvent.keyCode;
            waitingForKey = false;
        }
    }
    public void StartAssignment(string keyName)
    {
        if (!waitingForKey)
            StartCoroutine(AssignKey(keyName));
    }
    public void SendText(Text text)
    {
        buttonText = text;
    }
    IEnumerator WaitForKey()
    {
        while (!keyEvent.isKey)
            yield return null;
    }
    public IEnumerator AssignKey(string keyName)
    {
        waitingForKey = true;
        yield return WaitForKey();

        switch (keyName)
        {
            case "ShootermoveRightKey":
                GameManager.GM.ShooterRight = newKey;
                buttonText.text = GameManager.GM.ShooterRight.ToString();
                PlayerPrefs.SetString("ShootermoveRightKey", GameManager.GM.ShooterRight.ToString());
                break;
            case "ShootermoveLeftKey":
                GameManager.GM.ShooterLeft = newKey;
                buttonText.text = GameManager.GM.ShooterLeft.ToString();
                PlayerPrefs.SetString("ShootermoveLeftKey", GameManager.GM.ShooterLeft.ToString());
                break;
            case "ShooterJumpKey":
                GameManager.GM.ShooterJump = newKey;
                buttonText.text = GameManager.GM.ShooterJump.ToString();
                PlayerPrefs.SetString("ShooterJumpKey", GameManager.GM.ShooterJump.ToString());
                break;
            case "ShootKey":
                GameManager.GM.Shoot = newKey;
                buttonText.text = GameManager.GM.Shoot.ToString();
                PlayerPrefs.SetString("ShootKey", GameManager.GM.Shoot.ToString());
                break;
            case "TankRightKey":
                GameManager.GM.TankRight = newKey;
                buttonText.text = GameManager.GM.TankRight.ToString();
                PlayerPrefs.SetString("TankRightKey", GameManager.GM.TankRight.ToString());
                break;
            case "TankLeftKey":
                GameManager.GM.TankLeft = newKey;
                buttonText.text = GameManager.GM.TankLeft.ToString();
                PlayerPrefs.SetString("TankLeftKey", GameManager.GM.TankLeft.ToString());
                break;
            case "TankJumpKey":
                GameManager.GM.TankJump = newKey;
                buttonText.text = GameManager.GM.TankJump.ToString();
                PlayerPrefs.SetString("TankJumpKey", GameManager.GM.TankJump.ToString());
                break;
        }
        yield return null;
    }
    public void Options()
    {
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }
    public void ToMenu()
    {
        optionsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
