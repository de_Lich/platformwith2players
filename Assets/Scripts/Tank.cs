using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tank : MonoBehaviour
{
    private Controller controller;
    void Start()
    {
        controller = GetComponent<Controller>();
    }

    void Update()
    {
        if (Input.GetKey(GameManager.GM.TankRight))
        {
            controller.MoveRight();
        }
        if (Input.GetKey(GameManager.GM.TankLeft))
        {
            controller.MoveLeft();
        }
        if (controller.isGrounded && Input.GetKeyDown(GameManager.GM.TankJump))
        {
            controller.Jump();
        }
    }
}
