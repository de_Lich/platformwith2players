using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Winbox : MonoBehaviour
{
    [SerializeField] GameObject winMenu;
    [SerializeField] bool isPlayer = false;
    [SerializeField] bool isPlayer2 = false;
    private void Start()
    {
        winMenu.SetActive(false);
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Shooter")
            isPlayer = false;
        if (other.tag == "Tank")
            isPlayer2 = false;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Shooter")
        {
            isPlayer = true;
        }
        if (other.tag == "Tank")
        {
            isPlayer2 = true;
        }
    }
    private void Update()
    {
        if(isPlayer && isPlayer2)
        {
            Debug.Log("win");
            winMenu.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
