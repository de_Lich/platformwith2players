using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switcher2 : MonoBehaviour
{
    [SerializeField] GameObject switcher;
    private void Start()
    {
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        //switcher.GetComponent<Switcher>().isEntered = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        switcher.GetComponent<Switcher>().isEntered = false;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        switcher.GetComponent<Switcher>().isEntered = true;
    }
}
