using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float speed = 1.5f;
    public delegate void LooseDelegate();
    public static event LooseDelegate Loose;
    private void Update()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("rock"))
        {
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.CompareTag("Tank"))
        {
            Time.timeScale = 0;
            Loose();
        }
        Destroy(gameObject);
        Debug.Log("hit");
    }
}
