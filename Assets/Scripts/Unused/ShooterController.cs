using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterController : MonoBehaviour
{
    [SerializeField] float speed = 4f;
    [SerializeField] float jumpForce = 4f;
    [SerializeField] GameObject bullet;
    [SerializeField] Transform firePoint;

    private bool isGrounded = false;
    private Rigidbody2D rb;
    private SpriteRenderer sprite;
    [SerializeField] Vector3 bulletPos;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        bulletPos = new Vector2(0.7f, 1.5f);
    }

    
    void Update()
    {
        if (Input.GetKey(GameManager.GM.ShooterRight))
        {
            MoveRight();
        }
        if (Input.GetKey(GameManager.GM.ShooterLeft))
        {
            MoveLeft();
        }
        if (isGrounded && Input.GetKeyDown(GameManager.GM.ShooterJump))
        {
            Jump();
        }
        if (Input.GetKeyDown(GameManager.GM.Shoot))
        {
            Shoot();
        }
    }

    private void MoveLeft()
    {
        Vector3 dir = -transform.right * speed * Time.deltaTime;
        transform.Translate(dir);
        if(dir.x < 0.0f)
            transform.Rotate(0f, 180f, 0f);
        if (dir.x > 0.0f)
            transform.Rotate(0f, 0f, 0f);
    }

    private void FixedUpdate()
    {
        CheckGround();
    }
    private void MoveRight()
    {
        Vector3 dir = transform.right * speed * Time.deltaTime;
        transform.Translate(dir);
        if(dir.x > 0.0f)
            transform.Rotate(0f, 0f, 0f);
        if (dir.x < 0.0f)
            transform.Rotate(0f, 180f, 0f);
    }
    private void Jump()
    {
        rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    }
    private void CheckGround()
    {
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, 0.3f);
        isGrounded = collider.Length > 1;
    }
    private void Shoot()
    {
        Instantiate(bullet, firePoint.position, firePoint.rotation);
    }
}
