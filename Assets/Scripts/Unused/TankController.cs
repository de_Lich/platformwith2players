using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour
{
    [SerializeField] private float speed = 4f;
    [SerializeField] private float jumpForce = 4f;

    private bool isGrounded = false;
    private Rigidbody2D rb;
    private SpriteRenderer sprite;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    private void Update()
    {
        if (Input.GetKey(GameManager.GM.TankRight))
        {
            MoveRight();
        }
        if (Input.GetKey(GameManager.GM.TankLeft))
        {
            MoveLeft();
        }
        if(isGrounded && Input.GetKeyDown(GameManager.GM.TankJump))
        {
            Jump();
        }
    }

    private void Jump()
    {
        rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    }

    private void FixedUpdate()
    {
        CheckGround();
    }

    private void CheckGround()
    {
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, 0.3f);
        isGrounded = collider.Length > 1;
    }

    private void MoveRight()
    {
        Vector3 dir = transform.right * speed * Time.deltaTime;
        transform.Translate(dir);
        sprite.flipX = dir.x < 0.0f;                                       // may be not need
    }
    private void MoveLeft()
    {
        Vector3 dir = -transform.right * speed * Time.deltaTime;
        transform.Translate(dir);
        sprite.flipX = dir.x < 0.0f;                                                                               
    }
}
