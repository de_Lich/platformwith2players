using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadlyRock : MonoBehaviour
{
    public delegate void LoseDelegate();
    public static event LoseDelegate Loose;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Shooter"))
        {
            Time.timeScale = 0;
            Loose();
        }
        if (collision.gameObject.CompareTag("Tank"))
        {
            Time.timeScale = 0;
            Loose();
        }
    }
}
