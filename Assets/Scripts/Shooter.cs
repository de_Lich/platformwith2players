using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    [SerializeField] Transform firePoint;
    private Controller controller;
    private void Start()
    {
        controller = GetComponent<Controller>();
    }
    private void Update()
    {
        if (Input.GetKey(GameManager.GM.ShooterRight))
        {
            controller.MoveRight();
        }
        if (Input.GetKey(GameManager.GM.ShooterLeft))
        {
            controller.MoveLeft();
        }
        if (controller.isGrounded && Input.GetKeyDown(GameManager.GM.ShooterJump))
        {
            controller.Jump();
        }
        if (Input.GetKeyDown(GameManager.GM.Shoot))
        {
            Shoot();
        }
    }
    private void Shoot()
    {
        Instantiate(bullet, firePoint.position, firePoint.rotation);
    }
}
