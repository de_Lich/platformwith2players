using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;
    
    public KeyCode ShooterJump { get; set; }
    public KeyCode ShooterRight { get; set; }
    public KeyCode ShooterLeft { get; set; }
    public KeyCode Shoot { get; set; }
    public KeyCode TankJump { get; set; }
    public KeyCode TankLeft { get; set; }
    public KeyCode TankRight { get; set; }



    private void Awake()
    {
        if(GM == null)
        {
            DontDestroyOnLoad(gameObject);
            GM = this;
        }
        else if (GM != this)
        {
            Destroy(gameObject);
        }
        ShooterJump = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("ShooterJumpKey", "W"));
        ShooterRight = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("ShootermoveRightKey", "D"));
        ShooterLeft = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("ShootermoveLeftKey", "A"));
        Shoot = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("ShootKey", "Space"));

        TankJump = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("TankJumpKey", "UpArrow"));
        TankLeft = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("TankLeftKey", "LeftArrow"));
        TankRight = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("TankRightKey", "RightArrow"));
    }
}
