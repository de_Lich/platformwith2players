using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [SerializeField] private float speed = 4f;
    [SerializeField] private float jumpForce = 4f;

    public bool isGrounded = false;
    private Rigidbody2D rb;
    private SpriteRenderer sprite;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    public void Jump()
    {
        rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
    }

    private void FixedUpdate()
    {
        CheckGround();
    }

    private void CheckGround()
    {
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, 0.3f);
        isGrounded = collider.Length > 1;
    }

    public void MoveRight()
    {
        Vector3 dir = transform.right * speed * Time.deltaTime;
        transform.Translate(dir);
        if (dir.x > 0.0f)
            transform.Rotate(0f, 0f, 0f);
        if (dir.x < 0.0f)
            transform.Rotate(0f, 180f, 0f);                         
    }
    public void MoveLeft()
    {
        Vector3 dir = -transform.right * speed * Time.deltaTime;
        transform.Translate(dir);
        if (dir.x < 0.0f)
            transform.Rotate(0f, 180f, 0f);
        if (dir.x > 0.0f)
            transform.Rotate(0f, 0f, 0f);
    }
}
