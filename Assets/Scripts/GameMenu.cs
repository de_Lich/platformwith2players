using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMenu : MonoBehaviour
{
    private static GameMenu instance;
    public static GameMenu Instance { get { return instance; } }

    public GameObject looseMenu;
    public GameObject mainMenu;
    void Start()
    {
        looseMenu.SetActive(false);
        mainMenu.SetActive(false);
        Bullet.Loose += Loose;
        DeadlyRock.Loose += Loose;
    }

    public void CallMenu()
    {
        mainMenu.SetActive(!mainMenu.activeSelf);
        Debug.Log(mainMenu.activeSelf);
    }
    public void ToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
    private void Loose()
    {
        if (looseMenu != null)
        {
            looseMenu.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
