using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switcher : MonoBehaviour
{
    [SerializeField] GameObject rock;
    [SerializeField] private Vector3 upperPoint;
    [SerializeField] private Vector3 bottomPoint;

    private float speed = 2.0f;
    public bool isEntered = false;


    private void Awake()
    {
        rock.transform.position = new Vector3(-2f, -0.5f, 0f);
    }
    private void Update()
    {
        if (isEntered)
        {
            MoveUp();
        }
        if (!isEntered)
        {
            MoveDown();
        }
    }

    private void MoveDown()
    {
        rock.transform.position = Vector3.MoveTowards(rock.transform.position, bottomPoint, speed * Time.deltaTime);
    }

    private void MoveUp()
    {
        rock.transform.position = Vector3.MoveTowards(rock.transform.position, upperPoint, speed * Time.deltaTime);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(rock != null)
        {
            isEntered = false;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(rock != null)
        {
            isEntered = true;
        }
    }
}
